package main

import "github.com/gin-gonic/gin"
import "time"
import "fmt"
import "flag"

var request int = 0
var requestPing int = 0
var requestPong int = 0
var s int
func main() {
	flag.IntVar(&s, "s", 10, "延迟，默认为10")
	flag.Parse()
        f := fib()
	r := gin.Default()
	r.GET("/s", func(c *gin.Context) {
		sleep(s)
		c.JSON(200, gin.H{
			"message": "second",
			"delayed": s,
			"number": request,
		})
		request = request + 1
	})
	r.GET("/ping", func(c *gin.Context) {
		sleep(1)
		c.JSON(200, gin.H{
			"message": "ping",
			"delayed": "1s",
			"number": requestPing,
		})
		requestPing = requestPing + 1
	})
	r.GET("/pong", func(c *gin.Context) {
		sleep(2)
		c.JSON(200, gin.H{
			"message": "pong",
			"delayed": "2s",
			"number": requestPong,
		})
		requestPong = requestPong + 1
	})
	r.GET("/", func(c *gin.Context) {
		//print("Fibonacci:",f())
		//sleep(2)
		c.JSON(200, gin.H{
			"message": "ok",
			"Fibonacci": f(),
			"number": request,
		})
		request = request + 1
	})
	r.GET("/f10", func(c *gin.Context) {
		//print("Fibonacci:",f())
		//sleep(2)
		go f();
		go f();
		go f();
		go f();
		go f();
		go f();
		go f();
		go f();
		go f();
		go f();
		c.JSON(200, gin.H{
			"message": "ok",
			"Fibonacci": f(),
			"number": request,
		})
		request = request + 1
	})
	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
func fib() func() int {
	a, b := 0, 1
	return func() int {
		a, b = b, a+b
		return a
	}
}
func sleep(t int) {
    fmt.Println("begin")
    time.Sleep(time.Duration(t)*time.Second)
    fmt.Println("end")
}
